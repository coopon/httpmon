import os
import shutil
from setuptools import setup, find_packages

def prepare(dir_name='.httpmon/'):
    home_dir = os.getenv('HOME')
    if not os.path.isdir(home_dir + '/' + dir_name):
        os.mkdir(home_dir + '/' + dir_name)
    shutil.copy2('imgs/raise_alarm.svg', home_dir+'/'+dir_name+'/raise_alarm.svg')
    shutil.copy2('imgs/ok.svg', home_dir+'/'+dir_name+'/ok.svg')

prepare()


CLASSIFIERS = [
    'Intended Audience :: End Users/Desktop',
    'Environment :: Console',
    'License :: OSI Approved :: GNU General Public License (GPL)',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6'
]

setup(
    name='httpmon',
    version='0.1',
    license='GNU GPL v3',
    description='Check the status of your favorite websites.',
    long_description='supply url and get notified in your system about their status.',
    author = 'Prasanna Venkadesh',
    author_email = 'prasmailme@gmail.com',
    classifiers=CLASSIFIERS,
    packages=find_packages(),
    scripts=['scripts/httpmon'],
    install_requires=['notify2', 'aiohttp']
)
