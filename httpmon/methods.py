from urllib3.util import parse_url

async def check(url: str, session: object) -> tuple:
    '''pefrom HEAD requests for url and return the status'''
    try:
        async with session.head(url) as resp:
            status = resp.status
    except Exception as ex:
        print ("Exception Occurred: {}".format(ex))
        status = "Couldn't Connect"
    return (status, url)

def process_responses(responses: list) -> list:
    needs_attention = []
    for response in responses:
        status, url = response
        if status != 200:
            summary = "{} - {}".format(str(status), parse_url(url).host)
            needs_attention.append(summary)

    return needs_attention


